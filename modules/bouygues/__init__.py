# -*- coding: utf-8 -*-

# Copyright(C) 2019      Budget Insight


from .module import BouyguesModule


__all__ = ['BouyguesModule']
